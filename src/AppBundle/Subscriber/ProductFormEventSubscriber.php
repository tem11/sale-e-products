<?php
namespace AppBundle\Subscriber;

use Cocur\Slugify\Slugify;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ProductFormEventSubscriber implements EventSubscriberInterface
{
    private $slugifier;

    public static function getSubscribedEvents()
    {
        return [FormEvents::PRE_SUBMIT => 'preSubmit'];
    }

    public function preSubmit(FormEvent $formEvent)
    {
        $data = $formEvent->getData();
        if (isset($data['title'])) {
            $form = $formEvent->getForm();
            $form->add('slug', 'hidden');
            $form->get('slug')->setData($this->getSlugifier()->slugify($data['title']));
        }
    }

    private function getSlugifier()
    {
        if (!$this->slugifier) {
            $this->slugifier = new Slugify();
        }

        return $this->slugifier;
    }

}