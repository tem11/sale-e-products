<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends Controller
{
    /**
     * @Route(name="category_products", path="/category/products/{category}")
     */
    public function viewAction($category, Request $request)
    {
        $page = $request->get('page', 0);
        /** @var Category $category */
        $category = $this->get('app.repository.category')->findOneBy(['title' => $category]);

        return $this->render('@App/products/list_products.html.twig', [
            'pageTitle' => $category->getTitle(),
            'products' => $category->getProductsByPage($page),
            'pages' => $category->getPagesAmount(),
            'page' => $page,
            'category' => $category
        ]);
    }
}
