<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ProductsController extends Controller
{
    /**
     * @Route(path="/products", name="products_list")
     */
    public function indexAction(Request $request)
    {
        $repository = $this->get('app.repository.product');
        $page = $request->get('page',0);

        return $this->render('@App/products/list_products.html.twig', [
            'pageTitle' => 'Miscellaneous products overview',
            'products' => $repository->getProductsForPage($page),
            'pages' => $repository->getPagesAmount(),
            'page' => $page,
        ]);
    }

    /**
     * @Route(path="/products/{id}/view", name="product_view")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws NotFoundHttpException
     */
    public function viewAction($id)
    {
        $user = $this->getUser();
        /** @var Product $product */
        $product = $this->get('app.repository.product')->find($id);
        if (!$product || ($product->getStatus() !== Product::STATUS_ON_SALE &&
            $user !== $product->getOwner())
        ) {
            throw new NotFoundHttpException('Page not found');
        }

        return $this->render('@App/products/view.html.twig', [
            'product' => $product,
            'is_owned' => $user === $product->getOwner()
        ]);
    }

    /**
     * @Route(path="/products/search", name="product_search")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws NotFoundHttpException
     */
    public function searchAction(Request $request)
    {
        $page = $request->get('page', 0);
        $search = $request->get('search', '');
        $products = $this->get('app.repository.product')->searchProducts($search);
        $pages = 0;
        $products = $this->get('app.service.product')->paginate($products, $page, $pages);

        return $this->render('@App/products/list_products.html.twig', [
            'pageTitle' => 'Search results',
            'searched' => $search,
            'products' => $products,
            'page' => $page,
            'pages' => $pages,
            'additional_attributes' => ['search' => $search]
        ]);
    }

    /**
     * @Route(path="/products/filter", name="products_filter")
     */
    public function filterAction()
    {
        $products = $this->get('app.repository.product')->filterProducts(['human', 'kid']);

        return $this->render('@App/products/list_products.html.twig', [
            'pageTitle' => 'Search results',
            'products' => $products
        ]);
    }
}
