<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    const LANGUAGE_EN = 'en';
    const LANGUAGE_UA = 'ua';

    private $languages = [
        self::LANGUAGE_EN,
        self::LANGUAGE_UA,
    ];

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $categories = $this->get('doctrine.orm.entity_manager')->getRepository('AppBundle:Category')->findAll();

        return $this->render('@App/default/index.html.twig', ['categories' => $categories]);
    }

    /**
     * @Route(name="language_change", path="/language/{id}")
     * @param $id
     * @param Request $request
     * @return RedirectResponse
     * @throws \InvalidArgumentException
     */
    public function languageAction($id, Request $request)
    {
        if (!in_array($id, $this->languages, true)) {
            throw new \InvalidArgumentException('Unknown language id');
        }
        $request->setLocale($id);

        return new RedirectResponse($this->generateUrl('homepage'));
    }

    /**
     * @Route("/error", name="error_page")
     */
    public function errorAction(Request $request)
    {
        return $this->render(
            '@App/default/error.html.twig',
            ['message' => $request->get('message', 'Unknown error')]
        );
    }
}
