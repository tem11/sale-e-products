<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Router;

class PaymentController extends Controller
{
    /**
     * @Route(path="/payment/new/{id}", name="payment_create")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws NotFoundHttpException
     * @throws \LogicException
     */
    public function payAction($id)
    {
        /**
         * @var Product $product
         */
        $product = $this->get('app.repository.product')->find($id);
        if (!$product) {
            throw new NotFoundHttpException('Page not found');
        }
        $order = $this->get('app.service.payment_service')->createOrder($product, $this->getUser());
        $form = $this->get('app.service.payment_service')->getMerchantForm(
            $product,
            $order,
            $this->get('router')->generate('payment_success', [], Router::ABSOLUTE_URL),
            $this->get('router')->generate('payment_fail', [], Router::ABSOLUTE_URL)
        );

        return $this->render('@App/payment/payment.html.twig', [
            'form' => $form
        ]);
    }

    /**
     * @Route(path="/payment/success", name="payment_success")
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function successPaymentAction(Request $request)
    {
        $response = $this->get('app.service.payment_service')->merchantSourceVerification(
            $request->get('WMI_SIGNATURE'),
            $request->get('WMI_PAYMENT_NO'),
            $request->get('WMI_ORDER_STATE'),
            $request->attributes->all()
        );

        if (!$response) {
            $this->redirectToRoute('payment_fail');
        }

        $this->render('');
    }

    /**
     * @Route(path="/payment/fail", name="payment_fail")
     */
    public function failPaymentAction()
    {

    }
}
