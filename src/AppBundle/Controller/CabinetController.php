<?php
namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\ProductForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CabinetController
 * @package AppBundle\Controller
 *
 * @author Andrii Boichuk <andboychuk12@gmail.com>
 */
class CabinetController extends Controller
{
    /**
     * @Route(path="/profile/products/new", name="cabinet_new_product")
     * @Security(expression="has_role('ROLE_USER')")
     */
    public function newProductAction(Request $request)
    {
        $form = $this->get('form.factory')->create(ProductForm::class);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $formData = $form->getData();
            try {
                $this->get('app.service.product')->save(
                    $this->getUser(),
                    $formData['categoryId'],
                    $formData['title'],
                    $formData['description'],
                    $formData['licenseId'],
                    $formData['price'],
                    $formData['tags'],
                    $formData['imageFile'],
                    $formData['originFile']
                );
                return $this->redirectToRoute('cabinet_my_products');
            } catch (\Exception $exception) {
                $this->get('logger')->addWarning($exception->getMessage());
                return $this->redirectToRoute('error_page', ['message' => $exception->getMessage()]);
            }
        }

        return $this->render('@App/profile/new_product.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route(path="/profile/products/my", name="cabinet_my_products")
     * @Security(expression="has_role('ROLE_USER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function usersProductsAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $page = $request->get('page', 0);

        return $this->render('@App/products/list_products.html.twig', [
            'pageTitle' => 'My uploaded works',
            'products' => $user->getProductsByPage($page),
            'pages' => $user->getPagesAmount(),
            'page' => $page,
        ]);
    }

    /**
     * @Route(path="/products/download/origin/{id}", name="product_download")
     * @Security(expression="has_role('ROLE_USER')")
     * @param $id
     * @return Response
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     */
    public function downloadProductAction($id)
    {
        $product = $this->get('app.service.product')->findBoughtProduct($id, $this->getUser());
        $filename = $this->getParameter('protected_upload_dir') . DIRECTORY_SEPARATOR .  $product->getOrigin();

        $response = new Response();
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', mime_content_type($filename));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($filename) . '";');
        $response->headers->set('Content-length', filesize($filename));
        $response->sendHeaders();
        $response->setContent(file_get_contents($filename));

        return $response;
    }

    /**
     * @Route(path="/profile/products/purchased", name="purchased_products")
     * @Security(expression="has_role('ROLE_USER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function boughtProductsAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $page = $request->get('page', 0);
        $pages = 0;
        $products = $this->get('app.service.product')->paginate($user->getBoughtProducts(), $page, $pages);

        return $this->render('@App/products/list_products.html.twig', [
            'pageTitle' => 'Products that I\'ve bought',
            'products' => $products,
            'pages' => $user->getPagesAmount(),
            'page' => $page,
        ]);
    }
}
