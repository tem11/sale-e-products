<?php

namespace AppBundle\Controller;

use AppBundle\Entity\License;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class LicenseController extends Controller
{
    /**
     * @Route(name="license_listing", path="/license")
     */
    public function indexAction()
    {
        /** @var License[] $licenses */
        $licenses = $this->get('app.repository.license')->findAll();

        return $this->render('@App/licenses/index.html.twig', ['licenses' => $licenses]);
    }
}
