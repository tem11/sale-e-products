<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    const ROLE_ADMIN = 'ROLE_ADMIN';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="facebook_id", type="string", length=255, nullable=true)
     * @var string
     */
    private $facebookId;

    /**
     * @ORM\Column(name="google_id", type="string", length=255, nullable=true)
     * @var string
     */
    private $googleId;

    /**
     * @ORM\Column(name="vk_id", type="string", length=255, nullable=true)
     * @var string
     */
    private $vkId;

    /**
     * @ORM\Column(name="bio", type="text", nullable=true)
     * @var string
     */
    private $bio;

    /**
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     * @var string
     */
    private $firstName;

    /**
     * @ORM\Column(name="last_name", type="text", length=255, nullable=true)
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $facebookAccessToken;

    /**
     * @var string
     */
    private $googleAccessToken;

    /**
     * @var Product[]
     *
     * @ORM\OneToMany(targetEntity="Product", mappedBy="owner")
     */
    private $products;

    /**
     * @var ProductOrder[]
     *
     * @ORM\OneToMany(targetEntity="ProductOrder", mappedBy="user")
     */
    private $boughtProductOrders;

    /**
     * @var string
     */
    private $vkontakteAccessToken;

    public static $availableRoles = [
        self::ROLE_ADMIN => 'administrator',
        self::ROLE_SUPER_ADMIN => 'Super admin',
        self::ROLE_DEFAULT => 'Default user',
    ];

    /**
     * User constructor.
     * Constructor can not be dropped, without constructor definition in child class FOS will not work properly
     */
    public function __construct()
    {
        $this->products = new ArrayCollection();

        parent::__construct();
    }

    /**
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * @param string $facebookId
     * @return $this - returns this object, according to bundle standard
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    /**
     * @return string
     */
    public function getFacebookAccessToken()
    {
        return $this->facebookAccessToken;
    }

    /**
     * @param string $facebookAccessToken
     * @return $this - returns this object, according to bundle standard
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebookAccessToken = $facebookAccessToken;

        return $this;
    }

    /**
     * @return string
     */
    public function getGoogleId()
    {
        return $this->googleId;
    }

    /**
     * @param string $googleId
     * @return $this - returns this object, according to bundle standard
     */
    public function setGoogleId($googleId)
    {
        $this->googleId = $googleId;

        return $this;
    }

    /**
     * @return string
     */
    public function getVkId()
    {
        return $this->vkId;
    }

    /**
     * @param string $vkId
     * @return $this - returns this object, according to bundle standard
     */
    public function setVkId($vkId)
    {
        $this->vkId = $vkId;

        return $this;
    }

    /**
     * Alias for setVkId
     * @param $vkId
     * @return User
     */
    public function setVkontakteId($vkId)
    {
        return $this->setVkId($vkId);
    }

    /**
     * @return string
     */
    public function getGoogleAccessToken()
    {
        return $this->googleAccessToken;
    }

    /**
     * @param string $googleAccessToken
     * @return $this - returns this object, according to bundle standard
     */
    public function setGoogleAccessToken($googleAccessToken)
    {
        $this->googleAccessToken = $googleAccessToken;

        return $this;
    }

    /**
     * @return string
     */
    public function getVkontakteAccessToken()
    {
        return $this->vkontakteAccessToken;
    }

    /**
     * @param string $vkontakteAccessToken
     * @return $this - returns this object, according to bundle standard
     */
    public function setVkontakteAccessToken($vkontakteAccessToken)
    {
        $this->vkontakteAccessToken = $vkontakteAccessToken;

        return $this;
    }

    /**
     * @return Product[] of Product entity
     */
    public function getProducts()
    {
        if ($this->products instanceof Collection) {
            return $this->products->getValues();
        }

        return $this->products;
    }

    /**
     * @param $page
     * @return Product[]
     */
    public function getProductsByPage($page)
    {
        return array_slice($this->getProducts(), Product::DEFAULT_PAGE_SIZE * $page, Product::DEFAULT_PAGE_SIZE);
    }

    public function getPagesAmount()
    {
        return ceil(count($this->getProducts()) / Product::DEFAULT_PAGE_SIZE);
    }

    /**
     * @param ArrayCollection|Product[] $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @return string
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * @param string $bio
     */
    public function setBio($bio)
    {
        $this->bio = $bio;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return Product[]
     */
    public function getBoughtProducts()
    {
        $products = [];
        foreach ($this->getBoughtProductOrders() as $productOrder) {
            if ($productOrder->getStatus() === ProductOrder::STATUS_PAYED) {
                $products[] = $productOrder->getProduct();
            }
        }

        return $products;
    }

    /**
     * @param int $productId
     * @return bool
     */
    public function isProductBought($productId)
    {
        foreach ($this->getBoughtProductOrders() as $productOrder) {
            if ($productOrder->getStatus() === ProductOrder::STATUS_PAYED &&
                $productOrder->getProduct()->getId() === $productId) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return ProductOrder[]
     */
    public function getBoughtProductOrders()
    {
        return $this->boughtProductOrders;
    }

    /**
     * @param ProductOrder[] $boughtProductOrders
     */
    public function setBoughtProductOrders($boughtProductOrders)
    {
        $this->boughtProductOrders = $boughtProductOrders;
    }
}