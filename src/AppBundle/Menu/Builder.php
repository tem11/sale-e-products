<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');

        $menu->addChild('Home', ['route' => 'homepage']);
        $menu->addChild('Products', ['route' => 'products_list']);
        $menu->addChild('Licenses', ['route' => 'license_listing']);

        return $menu;
    }

    public function userMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav navbar-right');

        $user = $this->getUser();
        if ($user) {
            $menu->addChild('User', ['label' => 'Hi visitor'])
                ->setAttribute('dropdown', true)
                ->setAttribute('icon', 'fa fa-user');
            $menu['User']->addChild('Profile', ['route' => 'fos_user_profile_show'])
                ->setAttribute('icon', 'fa fa-view');
            $menu['User']->addChild('My products', ['route' => 'cabinet_my_products']);
            $menu['User']->addChild('Bought products', ['route' => 'purchased_products']);
            $menu['User']->addChild('New product', ['route' => 'cabinet_new_product']);
            $menu['User']->addChild("Logout({$user})", ['route' => 'fos_user_security_logout']);
        } else {
            $menu->addChild('Sign in', ['route' => 'fos_user_security_login']);
        }

        return $menu;
    }

    private function getUser()
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return null;
        }
        $token = $this->container->get('security.token_storage')->getToken();
        if (!$token) {
            return null;
        }

        return $token->getUser();
    }
}