<?php
namespace AppBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Class AppExtension
 * @package AppBundle\DependencyInjection
 *
 * @author Andrii Boichuk <andboychuk12@gmail.com>
 */
class AppExtension extends Extension
{
    const ALIAS = 'app';

    /**
     * @param array $configs
     * @param ContainerBuilder $container
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('forms.yml');
        $loader->load('repositories.yml');
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return self::ALIAS;
    }

}