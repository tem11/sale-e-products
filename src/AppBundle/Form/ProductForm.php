<?php

namespace AppBundle\Form;

use AppBundle\Repository\CategoryRepository;
use AppBundle\Repository\LicenseRepository;
use AppBundle\Repository\TagRepository;
use AppBundle\Subscriber\ProductFormEventSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductForm extends AbstractType
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var LicenseRepository
     */
    private $licenseRepository;
    /**
     * @var TagRepository
     */
    private $tagRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
        LicenseRepository $licenseRepository,
        TagRepository $tagRepository
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->licenseRepository = $licenseRepository;
        $this->tagRepository = $tagRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', 'text');
        $builder->add('description', 'textarea');
        $builder->add('categoryId', 'choice', [
            'choices' => $this->categoryRepository->getCategoriesForSelect()
        ]);
        $builder->add('licenseId', 'choice', [
            'choices' => $this->licenseRepository->getLicenseListForSelect()
        ]);
        $builder->add('tags', 'choice', [
            'choices' => $this->tagRepository->getTagListForSelect(),
            'multiple' => true
        ]);
        $builder->add('price', 'money', ['currency' => 'USD']);
        $builder->add('imageFile', 'file');
        $builder->add('originFile', 'file');

        $builder->addEventSubscriber(new ProductFormEventSubscriber());
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_product_form';
    }
}
