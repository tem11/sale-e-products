<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\BaseType;

class ArrayInput extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => [
                0 => 'Object 1',
                1 => 'Object 2',
            ]
        ]);
    }

    public function getParent()
    {
        return BaseType::class;
    }
}