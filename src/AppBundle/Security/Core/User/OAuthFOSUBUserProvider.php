<?php
namespace AppBundle\Security\Core\User;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseFOSUBProvider;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class MyFOSUBUserProvider
 * @package MyBundle\Security\Core\User
 *
 * @author Andrii Boichuk <andboychuk12@gmail.com>
 */
class OAuthFOSUBUserProvider extends BaseFOSUBProvider
{

    /**
     * @param UserInterface $user
     * @param UserResponseInterface $response
     *
     * @throws \RuntimeException
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        $property = $this->getProperty($response);
        $username = $response->getUsername(); // get the unique user identifier

        //we "disconnect" previously connected users
        $existingUser = $this->userManager->findUserBy(array($property => $username));
        if (null !== $existingUser) {
            // TODO set current user id and token to null for disconnect

            $this->userManager->updateUser($existingUser);
        }
        $this->userManager->updateUser($user);
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $userEmail = $response->getEmail();
        $user = $this->userManager->findUserByEmail($userEmail);

        // If user not found, create new one.
        if (null === $user) {
            $username = $response->getUsername();
            $user = $this->userManager->createUser();
            $service = $response->getResourceOwner()->getName();
            $idSetter = 'set' . ucfirst($service). 'Id';
            $user->$idSetter($username);
            $user->setUsername($username);
            $user->setEmail($userEmail);
            $user->setEnabled(true);
            $user->setPassword(uniqid(md5($username), false));
            $this->userManager->updateUser($user);
        }
        // update access token of user
        $serviceName = $response->getResourceOwner()->getName();
        $setter = 'set' . ucfirst($serviceName) . 'AccessToken';
        $user->$setter($response->getAccessToken());

        return $user;
    }
}