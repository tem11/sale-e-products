<?php

namespace AppBundle\Service;

use AppBundle\Entity\Product;
use AppBundle\Entity\ProductOrder;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PaymentService
 * @package AppBundle\Service
 *
 * @author Andrii Boichuk <andboychuk12@gmail.com>
 */
class PaymentService
{
    const MERCHANT_CURRENCY_ID = 840;

    const STATUS_ACCEPTED = 'ACCEPTED';
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var string
     */
    private $merchantId;

    /**
     * @var string
     */
    private $merchantSecret;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * PaymentService constructor.
     */
    public function __construct(EntityManager $entityManager, $merchantId, $merchantSecret, LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;
        $this->merchantId = $merchantId;
        $this->merchantSecret = $merchantSecret;
        $this->logger = $logger;
    }

    /**
     * @param $price
     * @return string
     */
    public function prepareSumForMerchant($price)
    {
        $money = explode('.', (string)$price);
        $major = $money[0];
        $minor = isset($money[1]) ? $money[1] : '00';
        if (strlen($minor) === 1) {
            $minor .= '0';
        }

        return "$major.$minor";
    }

    /**
     * @param Product $product
     * @param User $user
     * @return ProductOrder
     * @throws \InvalidArgumentException
     */
    public function createOrder(Product $product, User $user)
    {
        if (in_array($product, $user->getBoughtProducts(), false)||
            in_array($product, $user->getProducts(), false)
        ) {
            throw new \InvalidArgumentException('Product can not be bought');
        }
        $order = new ProductOrder();
        $order->setProduct($product);
        $order->setUser($user);
        $order->setPrice($this->prepareSumForMerchant($product->getPrice()));

        $this->entityManager->persist($order);
        $this->entityManager->flush($order);

        return $order;
    }

    /**
     * @param Product $product
     * @param ProductOrder $order
     * @param $successUrl
     * @param $failUrl
     * @return string
     */
    public function getMerchantForm(
        Product $product,
        ProductOrder $order,
        $successUrl,
        $failUrl
    )
    {
        $fields = [];

        $fields['WMI_MERCHANT_ID'] = $this->merchantId;
        $fields['WMI_PAYMENT_AMOUNT'] = $this->prepareSumForMerchant($order->getPrice());
        $fields['WMI_CURRENCY_ID'] = self::MERCHANT_CURRENCY_ID;
        $fields['WMI_PAYMENT_NO'] = $order->getId();
        $fields['WMI_DESCRIPTION'] = 'BASE64:' . base64_encode($product->getTitle());
        $fields['WMI_SUCCESS_URL'] = $successUrl;
        $fields['WMI_FAIL_URL'] = $failUrl;
        foreach ($fields as $name => $val) {
            if (is_array($val)) {
                usort($val, 'strcasecmp');
                $fields[$name] = $val;
            }
        }

        uksort($fields, 'strcasecmp');
        $fieldValues = '';

        foreach ($fields as $value) {
            if (is_array($value)) {
                foreach ($value as $v) {
                    $v = iconv('utf-8', 'windows-1251', $v);
                    $fieldValues .= $v;
                }
            } else {
                $value = iconv('utf-8', 'windows-1251', $value);
                $fieldValues .= $value;
            }
        }
        $signature = base64_encode(pack('H*', sha1($fieldValues . $this->merchantSecret)));
        $fields['WMI_SIGNATURE'] = $signature;
        $form = '<form class="auto-submit" action="https://wl.walletone.com/checkout/checkout/Index" method="POST">';
        foreach ($fields as $key => $val) {
            if (is_array($val)) {
                foreach ($val as $value) {
                    $form .= "$key: <input type='hidden' name='$key' value='$value'/>";
                }
            } else {
                $form .= "$key: <input type='hidden' name='$key' value='$val'/>";
            }
        }
        $form .= "<input class='btn btn-success' type='submit'/></form>";

        return $form;
    }

    /**
     * @param $inSignature
     * @param $paymentNumber
     * @param $orderState
     * @param array $requestData
     * @throws NotFoundHttpException
     * @throws OptimisticLockException
     * @return bool
     */
    public function merchantSourceVerification($inSignature, $paymentNumber, $orderState, $requestData)
    {
        foreach ($requestData as $name => $value) {
            if ($name !== 'WMI_SIGNATURE') {
                $params[$name] = $value;
            }
        }
        uksort($params, '');
        $values = '';

        foreach ($params as $name => $value) {
            $value = iconv('utf-8', 'windows-1251', $value);
            $values .= $value;
        }
        $signature = base64_encode(pack('H*', md5($values . $this->merchantSecret)));
        if ($signature === $inSignature) {
            if (strtoupper($orderState) !== self::STATUS_ACCEPTED) {
                $this->logger->warning("Wrong status: \'$orderState\'");
                return false;
            }
            $order = $this->entityManager->getRepository('AppBundle:ProductOrder')->find($paymentNumber);
            if (!$order) {
                throw new NotFoundHttpException();
            }
            $order->setStatus(ProductOrder::STATUS_PAYED);
            $this->entityManager->flush($order);
        } else {
            throw new NotFoundHttpException();
        }

        return false;
    }
}