<?php
namespace AppBundle\Service;

use AppBundle\Entity\Category;
use AppBundle\Repository\CategoryRepository;

class CategoryManagementService
{
    /**
     * @var CategoryRepository
     */
    private $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return Category[]
     */
    public function getCategoryAssociativeList()
    {
        return $this->repository->getCategoriesForSelect();
    }
}