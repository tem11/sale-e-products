<?php

namespace AppBundle\Service;

use AppBundle\Entity\User;
use Imagine\Exception\InvalidArgumentException;
use Imagine\Exception\RuntimeException;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\Point;
use Symfony\Component\HttpFoundation\File\File;
use Imagine\Gd\Image;

/**
 * Class FileUploadManager
 * @package AppBundle\Service
 *
 * @author Andrii Boichuk <andboychuk12@gmail.com>
 */
class FileUploadManager
{
    private $uploadDir;

    private $watermarkImage;

    private $imagine;
    private $privateUploadDir;

    public function __construct($uploadDir, $watermarkImage, $privateUploadDir)
    {
        $this->uploadDir = $uploadDir;
        $this->watermarkImage = $watermarkImage;
        $this->privateUploadDir = $privateUploadDir;
    }

    public function uploadImage(File $file, User $user)
    {
        $this->checkDirectories();
        $fileName = md5(uniqid($user->getUsername(), false) . $user->getId()) . '.' . $file->guessExtension();
        $imagineFile = $this->getImagine()->open($file->getRealPath());
        $imagineFile = $this->resizeImage($imagineFile, 1920);
        $this->addWatermark($imagineFile);

        $imagineFile->save($this->uploadDir . '/' . $fileName);

        $thumb = $this->resizeImage($imagineFile, 220);
        $thumb->save($this->uploadDir . '/thumbs/' . $fileName);

        return $fileName;
    }

    public function uploadOrigin(File $origin, User $user)
    {
        $originFileName = $user->getId(). '__' . md5(uniqid($user->getUsername(), false) . $user->getId()) . '.' . $origin->guessExtension();
        $origin->move($this->privateUploadDir, $originFileName);

        return $originFileName;
    }

    /**
     * @param Image $image
     * @param $maxSize
     * @return Image
     *
     * @throws RuntimeException
     * @throws InvalidArgumentException
     */
    public function resizeImage(Image $image, $maxSize)
    {
        $size = $image->getSize();
        $ratio = $size->getWidth() / $size->getHeight();
        if ($size->getWidth() > $maxSize || $size->getHeight() > $maxSize) {
            if ($ratio > 1) {
                $image->resize(new Box($maxSize, $maxSize / $ratio));
            } else {
                $image->resize(new Box($maxSize * $ratio, $maxSize));
            }
        }

        return $image;
    }

    public function addWatermark(ImageInterface $image)
    {
        $watermark = $this->getImagine()->open($this->watermarkImage);

        $size = $image->getSize();
        $wSize = $watermark->getSize();

        $bottomRight = new Point($size->getWidth() - $wSize->getWidth(), $size->getHeight() - $wSize->getHeight());

        $image->paste($watermark, $bottomRight);
    }


    /**
     * @throws \Exception
     */
    private function checkDirectories()
    {
        if (!@mkdir($this->uploadDir, 0755, true) && !is_dir($this->uploadDir)) {
            throw new \Exception('Can not create directory');
        }
        if (!@mkdir($this->uploadDirectory . '/thumbs', 0755, true) && !is_dir($this->uploadDir . '/thumbs')) {
            throw new \Exception('Can not create directory');
        }
    }

    /**
     * @return Imagine
     * @throws RuntimeException
     */
    private function getImagine()
    {
        if (!$this->imagine) {
            $this->imagine = new Imagine();
        }

        return $this->imagine;
    }
}