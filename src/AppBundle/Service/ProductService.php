<?php
namespace AppBundle\Service;

use AppBundle\Entity\Product;
use AppBundle\Entity\ProductOrder;
use AppBundle\Entity\User;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ProductService
 * @package AppBundle\Service
 *
 * @author Andrii Boichuk <andboychuk12@gmail.com>
 */
class ProductService
{
    /**
     * @var FileUploadManager
     */
    private $fileUploadManager;
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var Slugify
     */
    private $slugify;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(
        EntityManager $entityManager,
        FileUploadManager $fileUploadManager,
        Slugify $slugify,
        ValidatorInterface $validator
    ) {
        $this->fileUploadManager = $fileUploadManager;
        $this->entityManager = $entityManager;
        $this->slugify = $slugify;
        $this->validator = $validator;
    }

    /**
     * @param User $owner
     * @param $categoryId
     * @param $title
     * @param $description
     * @param $licenseId
     * @param $price
     * @param $tags
     * @param UploadedFile $image
     * @param UploadedFile $origin
     * @throws \InvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Symfony\Component\Validator\Exception\ValidatorException
     */
    public function save(
        User $owner,
        $categoryId,
        $title,
        $description,
        $licenseId,
        $price,
        $tags,
        UploadedFile $image,
        UploadedFile $origin
    ) {
        $license = $this->entityManager->getRepository('AppBundle:License')->find($licenseId);
        $category = $this->entityManager->getRepository('AppBundle:Category')->find($categoryId);
        if (!$license || !$category) {
            throw new \InvalidArgumentException('licenseId and categoryId must be valid values');
        }

        $product = new Product();
        $product->setCreateAt(new \DateTime());
        $product->setUpdatedAt(new \DateTime());
        $product->setCategory($category);
        $product->setSlug($this->slugify->slugify($title));
        $product->setDescription($description);
        $product->setLicense($license);
        $product->setPrice($price);
        $product->setTitle($title);
        $product->setOwner($owner);
        $product->setTags($tags);

        if ($this->validator->validate($product)) {
            $product->setHeadPicture($this->fileUploadManager->uploadImage($image, $owner));
            $product->setOrigin($this->fileUploadManager->uploadOrigin($origin, $owner));
            $this->entityManager->persist($product);
            $this->entityManager->flush($product);
            return;
        }

        throw new ValidatorException('Entity is not valid');
    }

    public function paginate($products, $page, &$pages)
    {
        $pages = ceil(count($products) / Product::DEFAULT_PAGE_SIZE);
        return array_slice($products, Product::DEFAULT_PAGE_SIZE * $page, Product::DEFAULT_PAGE_SIZE);
    }

    /**
     * @param $id
     * @param User $user
     * @return Product|null
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function findBoughtProduct($id, User $user)
    {
        $product = $this->entityManager->getRepository('AppBundle:Product')->find($id);
        foreach ($user->getBoughtProductOrders() as $productOrder) {
            if ($productOrder->getStatus() === ProductOrder::STATUS_PAYED &&
                $productOrder->getProduct()->getId() === (int)$id) {
                return $product;
            }
        }

        throw new NotFoundHttpException('Product not found');
    }
}