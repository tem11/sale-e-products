<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;


class FeedbackAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('id', 'text', ['read_only' => true]);
        $formMapper->add('name', 'text', ['read_only' => true]);
        $formMapper->add('description', 'text', ['read_only' => true]);
        $formMapper->add('topic', 'text', ['read_only' => true]);
        $formMapper->add('createdAt', 'text', ['read_only' => true]);
        $formMapper->add('isResolved', 'choice', ['choices' => [
            false => 'New',
            true => 'Resolved'
        ]]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('name');
        $datagridMapper->add('description');
        $datagridMapper->add('topic');
        $datagridMapper->add('isResolved', null, [], 'choice', ['choices' => [
            false => 'New',
            true => 'Resolved'
        ]]);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->addIdentifier('title');
        $listMapper->add('name', 'text');
        $listMapper->add('description', 'text');
        $listMapper->add('topic', 'text');
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
    }
}