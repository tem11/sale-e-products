<?php
namespace AppBundle\Admin;

use AppBundle\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class UserAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('id', 'text', ['read_only' => true]);
        $formMapper->add('email', 'text', ['read_only' => true]);
        $formMapper->add('username', 'text', ['read_only' => true]);
        $formMapper->add('roles', 'choice', [
            'multiple' => true,
            'choices' => User::$availableRoles
        ]);
        $formMapper->add('enabled', 'choice', [
            'choices' => [
                true => 'active',
                false => 'blocked'
            ]
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('email');
        $datagridMapper->add('username');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('id');
        $listMapper->addIdentifier('email');
        $listMapper->add('username');
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
    }
}