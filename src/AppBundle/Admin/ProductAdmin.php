<?php
namespace AppBundle\Admin;

use AppBundle\Entity\Product;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class ProductAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        //List of properties that can be edited by moderator
        $formMapper->add('category', 'sonata_type_model');
        $formMapper->add('status', 'choice', [
            'choices' => Product::$adminStatuses
        ]);

        $formMapper->add('license', 'sonata_type_model', [
            'read_only' => true,
            'disabled'  => true,
        ]);
        $formMapper->add('owner', 'sonata_type_model', [
            'read_only' => true,
            'disabled'  => true,
        ]);

        $formMapper->add('title', 'text', ['read_only' => true]);
        $formMapper->add('price', 'text', ['read_only' => true]);
        $formMapper->add('description', 'text', ['read_only' => true]);
        $formMapper->add('likes', 'text', ['read_only' => true]);
        $formMapper->add('createAt', 'datetime', [
            'read_only' => true,
            'disabled' => true
        ]);
        $formMapper->add('updatedAt', 'datetime', [
            'read_only' => true,
            'disabled' => true
        ]);
        $formMapper->add('slug', 'text', ['read_only' => true]);
        $formMapper->add('headPicture', 'text', ['read_only' => true]);

        /** @TODO Fix view of origin file */
//        $formMapper->add('originFile', 'text', ['read_only' => true]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
        $datagridMapper->add('status', null, [], 'choice', [
            'choices' => Product::$adminStatuses
        ]);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title');
        $listMapper->add('status', 'choice',  [
            'choices' => Product::$adminStatuses
        ]);
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
    }
}