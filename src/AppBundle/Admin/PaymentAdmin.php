<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class PaymentAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('id', 'text', ['read_only' => true]);
        $formMapper->add('orderId', 'text', ['read_only' => true]);
        $formMapper->add('amount', 'text', ['read_only' => true]);
        $formMapper->add('createAt', 'text', ['read_only' => true]);
        $formMapper->add('payerId', 'text', ['read_only' => true]);
        $formMapper->add('orderTitle', 'text', ['read_only' => true]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('orderId');
        $datagridMapper->add('createAt');
        $datagridMapper->add('payerId');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->addIdentifier('orderId');
        $listMapper->addIdentifier('createAt');
        $listMapper->addIdentifier('payerId');
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
        $collection->remove('edit');
    }
}