**# Sale E-products #**
=======
**Author:** Andrii Boichuk

### Technology stack:
1. Symfony 2.8 (LTS version)
    * SonataAdminBundle
    * FOSUserBundle
    * KNPMenuBundle
    * BraincraftedBootstrapBundle
    * AsseticBundle
2. MySQL
3. Memcached
4. JS, jQuery
5. HTML5
6. CSS3

### System requirements: ###
1. Web-server(nginx preferred)
2. Composer
3. PHP 5.6 or higher
4. Memecached
5. MySQL

### How to install project: ###
1. Clone repository
2. Perform composer install command
3. Open "<path-to-project>/app/config/parameters.yml" and set config parameters according 
to your environment 
4. Configure web server to point at "<path-to-project>/web" directory
5. Configure "hosts"(for unix OS "/etc/hosts") file and add server name(127.0.0.1 test.dev)
6. Restart nginx